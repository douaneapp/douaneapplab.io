# Douaneapp.com Changelog

## Unreleased


# v1.2.0 - 2020-05-03

 * Adds the GitterBadge component

# v1.1.0 - 2020-05-03

 * Adds i18n with English and French

# v1.0.0 - 2020-04-26

 * Initial release
