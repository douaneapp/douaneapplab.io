# Douaneapp.com

This repository is a React application served as douaneapp.com.

If you'd like to chat about Douane, join us on [![Gitter](https://badges.gitter.im/Douane/General.svg)](https://gitter.im/Douane/General?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)!

## Usage

* Clone this repo and run `yarn` in order to install all the dependencies.
* Finally boot the app with `yarn start`

It will open the local app in your favorite web browser.
